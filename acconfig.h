/****************************************************
 * Template for config.h.in
 * Undefine symbols that will be defined in config.h.
 ****************************************************/
#undef ENABLE_NLS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef HAVE_CATGETS
#undef HAVE_LIBSM
#undef PACKAGE
#undef VERSION
#undef PACKAGE_LOCALE_DIR
#undef PACKAGE_DATA_DIR
#undef PACKAGE_SOURCE_DIR
