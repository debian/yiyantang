/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/

/* Parse keyboard input. Trigger key: ctrl-y
 * Known commands:
 *		r:	Reset
 *		d:	Disable any conversion.
 *		a:	Autodetect
 *		g:	GB
 *		b:	BIG5
 *		h:	Hz
 *		u:	UCS
 *		7:	UTF-7
 *		8:	UTF-8
 *
 *		y:	output ctrl-y (So we will not lose the functionality 
 *			of ctrl-y, hopefully)
 *
 */
#include <stdio.h>
#include <string.h>
#include <hz.h>
#include "parseinput.h"
#include "yiyantang.h"

/* Shift buffer 1 char to left, lose the first char, reset 
 * the ret and the index.
 * ibuf points to the destination.
 * index is the offset from a bigger buffer.
 * ret is the length of the bigger buffer.
 */
void shiftbuf(char* ibuf, int* ret, int* index)
{
	memmove(ibuf, ibuf+1, sizeof(char)*(*ret - (*index+1)));
	/* ret always > 0, We don't want anything negative. */
	(*ret)--;
	if(*index > 0)
		(*index)--;
}	

/* Search ctrl-triggerKey which is not following a alt key.
 * Then we search known commands and set 'curconfig'. Found keys
 * are replace by a bell(\b). We can do a memmov but replace it 
 * with bell is easier. 'inputstate' indicate if we are in the
 * command mode.
 *
 * Oops, we use memmov now.
 */
void parse_input(char* ibuf, int* ret, yytconfig *curconfig, keystate *inputstate)
{
	int i;

	for(i=0; i < *ret; i++)
	{
		/* We don't want to screw alt-ctrl-key. */
		if(ibuf[i] == ALTKEY && !inputstate->cmdstate) 
		{
			inputstate->altstate = 1;
		}
		else 
		{
			if(!inputstate->altstate)
			{
				if(ibuf[i] == curconfig->yytesc)
				{
					inputstate->cmdstate = 1;
					shiftbuf(&ibuf[i], ret, &i);
					/* ibuf[i] = BEEP; */
				} /* YYTESC */

				else if(inputstate->cmdstate)
				{
					switch(ibuf[i])
					{
						case 'g':
							curconfig->fromcode = GB_CODE;
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
						case 'b':
							curconfig->fromcode = BIG5_CODE;
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
						case 'h':
							curconfig->fromcode = HZ_CODE;
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
						case 'u':
							curconfig->fromcode = UNI_CODE;
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
						case '7':
							curconfig->fromcode = UTF7_CODE;
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
						case '8':
							curconfig->fromcode = UTF8_CODE;
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
						case 'd':	/* Disable conversion. */
							curconfig->fromcode = -2;
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
						case 'a':	/* autoconversion. */
							curconfig->fromcode = 0;
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
						case 'r':	/* reset the initial state. */
							curconfig->fromcode = -1;
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
						case 'y':	/* write the escape key. */
							ibuf[i] = curconfig->yytesc;
							break;
						default:
							shiftbuf(&ibuf[i], ret, &i);
							/* ibuf[i] = BEEP; */
							break;
					}	/* switch */
					inputstate->cmdstate = 0; /* Reset the state. */
				} /* else if cmdstate. */

			} /* if !altstate. */
			inputstate->altstate = 0;
		} /* else ALTKEY. */
	} /* for loop. */
}






/* vi: set ts=4:sw=4: */
