/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/

#include <unistd.h>
#include <stdio.h>
#include "hzsegment.h"

int main ()
{
    char *pbuf;
    //  char *test = "老虎不吃人 This is not really a a大事";
    char *test = "->  -   41:[/2�塀IG5-�塀IG5-ネ冠ほ獁B]  3 cjk /2�塀IG5-�塀IG5-いゅぃㄓ0000 ";
    int ret, count;

    ret = 0;
    count = strlen (test);
    pbuf = test;
    printf ("count: %d, ret: %d\n", count, ret);
    for (;;)
    {
	ret = hzsegment (pbuf, count);
	write (STDOUT_FILENO, pbuf, ret);
	write (STDOUT_FILENO, "\n", 1);
	count -= ret;
	pbuf += ret;
	printf ("count: %d, ret: %d\n", count, ret);
	if (count <= 0)
	    return 0;
    }
	return 0;
}


/* vi: set ts=4:sw=4: */
