/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/
/*
 * Main program. Calls yiyantang_init() to do initialization.
 * Then create psuedo terminal, start a command in child and
 * start main loop in parent.
 * */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <stropts.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <pty.h>
#include <signal.h>

#include "loop.h"
#include "setty.h"
#include "yyt_util.h"
#include "yiyantang_init.h"

int global_fdm;

void sig_winch(int signo)
{
	struct winsize size;
	
	if(ioctl(STDIN_FILENO, TIOCGWINSZ, (char*) &size) < 0)
		err_sys("TIOCGWINSIZE error");
	tty_resize(global_fdm, &size);
	signal(SIGWINCH, sig_winch);
}

int main (int argc, char *argv[])
{
    int fdm;
    int ignoreeof;
    pid_t pid;
    struct winsize size;
    struct termios orig_termios;
    int fromcode, tocode;
    char **cmd;

    ignoreeof = 0;

    cmd = yiyantang_init (argc, argv, &fromcode, &tocode);
    if (tcgetattr (STDIN_FILENO, &orig_termios) < 0)
	err_sys ("tcgettattr error on stdin");
    if (ioctl (STDIN_FILENO, TIOCGWINSZ, (char *) &size) < 0)
	err_sys ("TIOCGWINSZ error");
    pid = forkpty (&fdm, NULL, &orig_termios, &size);

    if (pid < 0)
	err_sys ("fork error");

    else if (pid == 0)
    {
	printf ("Start a shell...\n");
	if (execvp (cmd[0], cmd) < 0)
	    err_sys ("cannot execute: %s", cmd[0]);

    }				/* End child. */

	global_fdm = fdm;

    if (tty_raw (STDIN_FILENO) < 0)
	err_sys ("can't set STDIN_FILENO to raw");
    if (atexit (tty_atexit) < 0)
	err_sys ("atexit tty won't set");
	signal(SIGWINCH, sig_winch);

    loop (fdm, fromcode, tocode);
	/* Not really need to wait but whatever. */
	wait(NULL);
    exit (0);
}


/* vi: set ts=4:sw=4: */
