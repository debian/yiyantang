/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/
#ifndef _YIYANTANG_H_
#define _YIYANTANG_H_

#define CONTROL(x)		((x)&(0x1F))
#define YYTESC		CONTROL('y')
#define ALTKEY		0x1B
#define BEEP		0x07

enum {
	TRUE = 1,
	FALSE = 0,
};

struct _yytconfig {
	int tocode;
	int fromcode;
	int yytesc;			/* escape key value. */
	int isnewbuf;		/* New buffer state. */
	char** shellcmd;	/* char *[] of command line list. */
};

typedef struct _yytconfig yytconfig;

struct _keystate {
	int altstate;
	int cmdstate;
};
typedef struct _keystate keystate;

void sig_winch(int signo);

#endif /* _YIYANTANG_H_ */


/* vi: set ts=4:sw=4: */
