/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/
/*
 * Do initialization here. Actually only do command line parsing.
 */

#include <locale.h>
#include <langinfo.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <hz.h>

#include "cmdline.h"
#include "zhinit.h"

#if ENABLE_NLS
# include <libintl.h>
#define _(Text) gettext(Text)
#else
#define textdomain(Domain)
#define _(Text) Text
#endif
#define N_(Text) Text

void printusage (void);

char** getshell ()
{
	char *shellcmd;
	char **cmd;

	cmd = malloc (sizeof (char *) * 3);

	if ((shellcmd = getenv ("SHELL")) == NULL)
		shellcmd = strdup ("/bin/sh");

	cmd[0] = shellcmd;
	cmd[1] = strdup ("-i");
	cmd[2] = NULL;
	return cmd;
}

int gettocode ()
{
	char *ret;

	if ((ret = setlocale (LC_CTYPE, "")) == NULL)
	{
		return (0);
	}

	if ((ret = nl_langinfo (CODESET)) == NULL)
	{
		return (0);
	}

	if (!strncasecmp (ret, "GB2312", 6))
	{
		return (GB_CODE);
	}
	if (!strncasecmp (ret, "GBK", 3))
	{
		return (GB_CODE);
	}
	else if (!strncasecmp (ret, "BIG5", 4))
	{
		return (BIG5_CODE);
	}
	else if (!strncasecmp (ret, "UTF8", 4))
	{
		return (UTF8_CODE);
	}
	else
		return (0);
}

void parseencode (char *input, int *encode)
{

	if (!strncasecmp (input, "GB", 2))
	{
		*encode = GB_CODE;
		_initgb ();
	}
	else if (!strncasecmp (input, "BIG5", 4))
	{
		*encode = BIG5_CODE;
		_initbig ();
	}
	else if (!strncasecmp (input, "UTF8", 4))
	{
		*encode = UTF8_CODE;
		_initutf8 ();
	}
	else
	{
		printf ("encoding %s is not supported.\n", input);
		printusage ();
	}
}

void printusage ()
{
	printf ("\n\
Usage: yyt [-f fromencode -t toencode] [-h] [program arg....].\n\
\n\
	Default is to start an interactive shell.\n\
	If no -t is specified, encoding from LC_CTYPE will be used.\n\
	Supported Encoding: gb, big5, utf8. \n\n");
		exit (0);
}

char** yiyantang_init (int argc, char **argv, int *fromcode, int *tocode)
{
	struct gengetopt_args_info args_info;
/*	int c;
 */
	char **cmd;

	*fromcode = 0;
	*tocode = gettocode ();
	cmd = getshell ();

	hz_setup();
#if 0
	opterr = 0;			/* getopt variable. */
	while ((c = getopt (argc, argv, "+f:t:")) != -1)
	{
		switch (c)
		{
			case 'f':
				parseencode (optarg, fromcode);
				break;
			case 't':
				parseencode (optarg, tocode);
				//printf ("tocode: %d\n", *tocode);
				break;
			case 'h':
			case '?':
				printusage ();
		}
	}
	
	if (optind < argc)
	{
		cmd = &argv[optind];
	}
#endif /* 0 */
	if(cmdline_parser(argc, argv, &args_info) != 0)
	{
		cmdline_parser_print_help();
		cmdline_parser_print_version();
		exit(1);
	}
	if(args_info.from_given)
		parseencode(args_info.from_arg, fromcode);
	if(args_info.to_given)
		parseencode(args_info.to_arg, tocode);
	if(args_info.inputs_num)
		cmd = args_info.inputs;

	return (cmd);

}


/* vi: set ts=4:sw=4: */
