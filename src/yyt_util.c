/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/
/* Help functions from APUE */

#include <signal.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "yyt_util.h"

#define MAXLINE		4096

static void err_doit (int errnoflag, const char *fmt, va_list ap)
{
    int errno_save;
    char buf[MAXLINE];

    errno_save = errno;
    vsprintf (buf, fmt, ap);
    if (errnoflag)
		sprintf (buf + strlen (buf), ": %s", strerror (errno_save));
    strcat (buf, "\n");
    fflush (stdout);
    fputs (buf, stderr);
    fflush (NULL);
    return;
}

void err_sys (const char *fmt,...)
{
    va_list ap;
    va_start (ap, fmt);
    err_doit (1, fmt, ap);
    va_end (ap);
    exit (1);
}

Sigfunc *signal_intr (int signo, Sigfunc * func)
{
    struct sigaction act, oact;

    act.sa_handler = func;
    sigemptyset (&act.sa_mask);
    act.sa_flags = 0;
#ifdef SA_INTERRUP		/* SunOS */
    act.sa_flags |= SA_INTERRUPT;
#endif
    if (sigaction (signo, &act, &oact) < 0)
	return (SIG_ERR);
    return (oact.sa_handler);
}

ssize_t writen (int fd, const void *vptr, size_t n)
{
    size_t nleft;
    ssize_t nwritten;
    const char *ptr;

    ptr = vptr;
    nleft = n;
    while (nleft > 0)
    {
	if ((nwritten = write (fd, ptr, nleft)) <= 0)
	    return (nwritten);
	nleft -= nwritten;
	ptr += nwritten;
    }
    return (n);
}



/* vi: set ts=4:sw=4: */
