/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/
#ifndef _UTIL_H_
#define _UTIL_H_

void err_sys (const char *fmt,...);
typedef void Sigfunc (int);
Sigfunc *signal_intr (int signo, Sigfunc * func);
ssize_t writen (int fd, const void *vptr, size_t n);

#endif /* _UTIL_H_ */


/* vi: set ts=4:sw=4: */
