/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/

/*
 * Do Chinese conversion here.
 * Well hopeful, future version of libhz will do this for us.
 */

/* Damn, This half hanzi at the end is killing me. Need to
 * know the state of it. Update j_code() when libhz expose
 * half hanzi state.
 * */
#ifndef _HZ_H_
#define _HZ_H_
#include <hz.h>
#endif /* _HZ_H_ */

#include <stdio.h>
#include "zhconv.h"
#include "yiyantang.h"

/* When fromcode = 0, we guess encoding ourselves. 
 * fromcode = -2, disable conversion. 
 * */
int zhconv (yytconfig curconfig, char *buf, int *count)
{
	int fromcode, tocode;
	static int lastfrom = 0;	/* From code from the last iteration. */
	fromcode = curconfig.fromcode;
	tocode = curconfig.tocode;
	

	if (fromcode == -2 || tocode == fromcode)
	{
		return *count;
	}
	if (fromcode == 0)
	{
		if ((fromcode = j_code (buf, *count)) == tocode)
			return *count;
	}

	/* hz_search will reset half-hanzi everytime. 
	 * We don't want that at the buffer boundary if encoding
	 * is not changed.
	 */
	if(!(curconfig.isnewbuf == TRUE && fromcode == lastfrom))
	{
		lastfrom = fromcode;
		if(! hz_search(fromcode, tocode, 8))
		{
			/* failed to find a valid conversion path. */
			return *count;
		}
	}
	
	hz_convert(buf, count, 0);
	return *count;

}


/* vi: set ts=4:sw=4: */
