/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/

#ifndef _ZHCONV_H_
#define _ZHCONV_H_

/* hz.h did not define _HZ_H_ */
#ifndef _HZ_H_
#define _HZ_H_
#include <hz.h>
#endif /* _HZ_H_ */

#include "yiyantang.h"

enum {
	BUFFSIZE = 	MAX_BUFFER,	/* Input buffer size, defined in hz.h */
	PRESIZE = 	1,			/* Prepending bytes before input buffer. */
};


int zhconv (yytconfig curconfig, char *buf, int *count);

#endif /* _ZHCONV_H_ */


/* vi: set ts=4:sw=4: */
