/************************************************************************** 
 * yyt - Psuedo tty converts amoung different Chinese encodings.
 *
 * Copyright (C) 2001 ha shao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 **************************************************************************/

/* Initialize convert functions based on destiny encoding. 
 * So initialize everything for one encoding. Crude but so what?
 * */

#include <hz.h>

#include "zhinit.h"

enum zhinitcode
{ GBBIG5, GBHZ, GBUNI,
    BIG5GB, BIG5UNI,
    HZGB,
    UNIGB, UNIBIG5, UNIUTF7, UNIUTF8,
    UTF7UNI,
    UTF8UNI,
    MAXZHCODE
};

int init_stat[MAXZHCODE];

void _initgb ()
{
    if (!init_stat[HZGB])
    {
	hz2gb_init ();
	init_stat[HZGB] = 1;
    }
    if (!init_stat[BIG5GB])
    {
	big2gb_init ();
	init_stat[BIG5GB] = 1;
    }
    if (!init_stat[UNIGB])
    {
	uni2gb_init ();
	init_stat[UNIGB] = 1;
    }
}

void _initbig ()
{
    if (!init_stat[GBBIG5])
    {
	gb2big_init ();
	init_stat[GBBIG5] = 1;
    }
    if (!init_stat[UNIBIG5])
    {
	uni2big_init ();
	init_stat[UNIBIG5] = 1;
    }
}

void _inithz ()
{
    if (!init_stat[GBHZ])
    {
	gb2hz_init ();
	init_stat[GBHZ] = 1;
    }
}

void _inituni ()
{
    if (!init_stat[GBUNI])
    {
	gb2uni_init ();
	init_stat[GBHZ] = 1;
    }
    if (!init_stat[BIG5UNI])
    {
	big2uni_init ();
	init_stat[BIG5UNI] = 1;
    }
    if (!init_stat[UTF7UNI])
    {
	utf7_uni_init ();
	init_stat[UTF7UNI] = 1;
    }
    if (!init_stat[UTF8UNI])
    {
	utf8_uni_init ();
	init_stat[UTF8UNI] = 1;
    }
}

void _initutf7 ()
{
    if (!init_stat[UNIUTF7])
    {
	uni_utf7_init ();
	init_stat[UNIUTF7] = 1;
    }
}

void _initutf8 ()
{
    if (!init_stat[UNIUTF8])
    {
	uni_utf8_init ();
	init_stat[UNIUTF8] = 1;
    }
}


/* vi: set ts=4:sw=4: */
